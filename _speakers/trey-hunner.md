---
name: Trey Hunner
talks:
- 'Loop better: a deeper look at iteration in Python'
---

Trey Hunner is a Python/Django teacher, trainer, and mentor. He is a director at the Python Software Foundation, a member of the Django Software Foundation, and is heavily involved with his local Python meetup group in San Diego.

Trey holds corporate training sessions and Python and Django by day and hosts live online teaching sessions for Python learners during nights and weekends.