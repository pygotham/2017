---
name: Angela Fox
talks:
- Create Single Python Executables for Easy Deployment
---

Angela Fox is a back-end software engineer building FINDMINE’s award-winning  “Automated Complete the Look” solution, which uses machine learning to scale product curation. 

In collaboration with CTO Konstantin Itskov, Angela spearheads the design, development, testing and deployment of new products at FINDMINE. She also contributes her expertise in database administration, system administration, and technical documentation. As FINDMINE’s  product owner, Angela ensures that the vision and goals of the FINDMINE software product are communicated, planned and executed in a timely manner.

As a developer, Angela has had the opportunity to create numerous exciting features for FINDMINE. Her recent work formed the foundations for the Robot Outfits functionality, improved FINDMINE’s ability to correctly classify items and refined the internal messaging protocol for the FINDMINE system. Angela’s accomplishments have helped propel FINDMINE to the forefront of retail innovation: FINDMINE has been recognized by numerous publications awards and is experiencing rapid growth.

Prior to joining FINDMINE upon her graduation from Columbia University’s with a degree in computer science, Angela earned Le Diplome de Cuisine from Le Cordon Bleu in London and worked as a professional chef in New York City. Angela is also a veteran of the U.S. Air Force, with over three years of active duty service, during which time she had the opportunity to live in Russia and deploy to Ali Al Salem Air Base, Kuwait.  

Angela’s presentation at PyGotham is similar to one she gave at PyCon earlier this year at the Intel Booth. In her presentation, she will discuss a unique approach to facilitating the deployment of Python applications by using two build tools to bundle the necessary third party dependencies, creating a single Python executable for easy deployment in any environment.