---
abstract: 'The advent of sophisticated online services, has resulted in an unprecedented
  generation of textual content making NLP a fundamental tool in any data scientists
  toolkit. Here we introduce the rudiments of Natural Language Processing, from counting
  words to topic modeling and language detection. '
duration: 40
level: Intermediate
presentation_url: https://www.slideshare.net/NoemiDerzsy/pygotham-ny-2017-natural-language-processing-from-scratch
room: PennTop North
slot: 2017-10-07 11:15:00-04:00
speakers:
- "Bruno Gon\xE7alves"
- Noemi Derzsy
title: Natural Language Processing from Scratch
type: talk
video_url: https://youtu.be/lNPXlqtJwcQ
---

We introduce the fundamental technique of natural language processing using Python and OpenNasa datasets. In particular: 

* bag of words models
* stop words
* tf/idf
* basic topic modeling
* word clouds
* language detection

A GitHub repository will be made available with all the code and slides used during the talk.